import React from "react";

import { createTheme, ThemeProvider } from '@mui/material/styles';

import { Button, TextField } from "@mui/material";

import { Bounce } from "react-activity";
import "react-activity/dist/library.css";

const theme = createTheme({
    status: {
      danger: '#e53e3e',
    },
    palette: {
      primary: {
        main: '#15171c',
        darker: '#000000',
      },
      neutral: {
        main: '#64748B',
        contrastText: '#fff',
      },
    },
});

export const CustomButton = ({
    placeholder,
    isLoading,
    ...props
 }) => {
     return (
        <ThemeProvider theme={theme}>
            <Button style={{minHeight: 40}} {...props}>{isLoading ? <Bounce/> : placeholder}</Button>
        </ThemeProvider>
     )
}

export const CustomInput = ({
  ...props
}) => {
   return (
      <ThemeProvider theme={theme}>
          <TextField {...props}/>
      </ThemeProvider>
   )
}