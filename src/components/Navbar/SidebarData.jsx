import React from "react";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import * as RiIcons from "react-icons/ri";

import { IoMdPersonAdd, IoIosPaper } from "react-icons/io";

import { GoDiffAdded } from "react-icons/go";
import { MdSell } from "react-icons/md";
import { AiTwotoneEdit } from "react-icons/ai";
import { AiFillSchedule } from "react-icons/ai";

export const SidebarData = [
  {
    title: "Início",
    path: "/inicio",
    icon: <AiIcons.AiFillHome />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />
  },
  {
    title: "Cadastros",
    icon: <IoIcons.IoIosPaper />,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,
    path: "#",
    subNav: [
      {
        title: "Cadastro de produto",
        path: "/cadastroProduto",
        icon: <GoDiffAdded/>,
        cName: "sub-nav"
      },
      {
        title: "Cadastro de serviço",
        path: "/cadastroServico",
        icon: <IoIosPaper/>,
        cName: "sub-nav"
      },
      {
        title: "Cadastro de cliente",
        path: "/cadastroCliente",
        icon: <IoMdPersonAdd/>
      }
    ]
  },
  {
    title: "Vendas",
    icon: <MdSell/>,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,
    path: "#",
    subNav: [
      {
        title: "Registrar venda",
        path: "/registroVenda",
        icon: <MdSell/>,
        cName: "sub-nav"
      },
      {
        title: "Histórico de vendas",
        path: "/historicoVendas",
        icon: <MdSell/>,
        cName: "sub-nav"
      }
    ]
  },
  {
    title: "Custos",
    path: "/lancamentoCusto",
    icon: <MdSell/>,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />
  },
  {
    title: "Edição de cadastros",
    path: "/edicaoCadastros",
    icon: <AiTwotoneEdit/>,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />
  },
  {
    title: "Agenda",
    path: "/agenda",
    icon: <AiFillSchedule/>,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />
  },
];