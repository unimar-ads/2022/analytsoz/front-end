import React, { useState } from 'react';
import styled from 'styled-components';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { SidebarData } from './SidebarData';
import SubMenu from "./SubMenu";
import { IconContext } from 'react-icons/lib';
import { IoMdPerson, IoIosLogOut } from "react-icons/io";
import { useNavigate } from "react-router-dom";

import IconButton from '@mui/material/IconButton';

import { logout } from "../../controllers/AuthController";

const Nav = styled.div`
  background: #15171c;
  height: 80px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const NavIcon = styled.a`
  margin-left: 2rem;
  font-size: 2rem;
  height: 80px;
  display: flex;
  cursor: pointer;
  justify-content: flex-start;
  align-items: center;
`;

const SidebarNav = styled.nav`
  background: #15171c;
  min-width: 300px;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  position: fixed;
  top: 0;
  left: ${({ sidebar }) => (sidebar ? "0" : "-100%")};
  transition: 100ms;
  z-index: 10;
`;

const SidebarWrap = styled.div`
  width: 100%;
`;

export default function Sidebar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const navigate = useNavigate();

  const logoutReq = async() => {
    logout();
    localStorage.removeItem("nome");
    navigate("/login");
  }

  return (
    <>
      <IconContext.Provider value={{ color: "#fff" }}>
        <Nav style={{flexDirection: "row", justifyContent: "space-between" }}>
            <NavIcon>
              <FaIcons.FaBars onClick={showSidebar} />
            </NavIcon>
            <IconButton onClick={logoutReq} style={{marginRight: 20}}>
              <IoIosLogOut size={35}/>
            </IconButton>
        </Nav>
        <SidebarNav sidebar={sidebar}>
          <SidebarWrap>
            <NavIcon>
              <AiIcons.AiOutlineClose onClick={showSidebar} />
            </NavIcon>
            <div style={{marginBottom: 10, marginLeft: 16, display: "flex", flexDirection: "row", alignContent: "center"}}>
              <IoMdPerson style={{width: 19, height: 19}}/>
              <h1 style={{color: "#fff", fontSize: 16, marginLeft: 5}}>{localStorage.getItem("nome")}</h1>
            </div>
            {SidebarData.map((item, index) => {
              return <SubMenu showSidebar={showSidebar} item={item} key={index} />;
            })}
          </SidebarWrap> 
        </SidebarNav>
      </IconContext.Provider>
    </>
  );
}
