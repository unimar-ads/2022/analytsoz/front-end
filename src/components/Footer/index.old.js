import React from "react";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
} from "./style";
  
const Footer = () => {
  return (
    <Box>
      <h1 style={{ color: "white", 
                   textAlign: "center", 
                   marginTop: "-40px" }}>
        AnalytcOZ - Todos os direitos reservados
      </h1>
      <Container style={{textAlign: "left",}}>
        <Row>
          <Column>
            <Heading>Sobre nós </Heading>
            <FooterLink href="/criacao">Criação</FooterLink>
            <FooterLink href="/visao">Visão</FooterLink>
          </Column>
          <Column>
            <Heading>Contate-nos</Heading>
            <FooterLink>analytcoz2022@gmail.com</FooterLink>
            <FooterLink>Tel(contato zap comercial se tiver)</FooterLink>
          </Column>
        </Row>
      </Container>
    </Box>
  );
};
export default Footer;