import React from "react";

import { useFormik } from "formik";
import * as yup from "yup";

import { Paper, Stack } from "@mui/material";

import Logo from "../../assets/logo_branca.png";
import styles from "./styles.js";

import { login } from "../../controllers/AuthController";

import { useNavigate } from "react-router-dom";

import { CustomButton, CustomInput } from "../../components/CustomWidgets";

import { useSnackbar } from "notistack";

const validationSchema = yup.object({
  usuario: yup.string().trim().required("Informe seu usuário"),
  senha: yup.string().trim().required("Informe sua senha")
});

export default function Login() {
  const { enqueueSnackbar } = useSnackbar();
  const [loading, setLoading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      usuario: "",
      senha: ""
    },
    onSubmit: loginRequest,
    validationSchema: validationSchema
  });

  const navigate = useNavigate();

  async function loginRequest(values) {
    setLoading(true);
    const response = await login(values.usuario, values.senha);
    setLoading(false);

    if (response) {
      navigate("/inicio");
    } else {
      enqueueSnackbar("Usuário ou senha incorreto(s)",  {variant: "error"});
    }
  }

  return ( 
      <>
        <img style={styles.img} src={Logo}/>
        <form onSubmit={formik.handleSubmit}>
            <Paper sx={{ padding: 4, maxWidth: 340, justifyContent: "center", alignItems: "center", maxHeight: 400, position: "absolute", marginLeft: "auto", marginRight: "auto", left: 0, right: 0, top: 300 }}>
                <Stack spacing={2}>
                    <CustomInput 
                      name="usuario"
                      value={formik.values.usuario}
                      onChange={formik.handleChange}
                      error={formik.touched.usuario && Boolean(formik.errors.usuario)}
                      helperText={formik.touched.usuario && formik.errors.usuario}
                      color="primary"  
                      label="Usuário" 
                      variant="outlined" 
                    />
                    <CustomInput
                      name="senha"
                      value={formik.values.senha}
                      onChange={formik.handleChange}
                      error={formik.touched.senha && Boolean(formik.errors.senha)}
                      helperText={formik.touched.senha && formik.errors.senha}
                      type="password" 
                      color="primary" 
                      label="Senha" 
                      variant="outlined" 
                    />    
                    <CustomButton type="submit" isLoading={loading} placeholder={"Entrar"} color="primary" variant="contained"/>
                </Stack>
            </Paper>
        </form>
        <div style={{display: "flex", justifyContent: "center", alignItems: "center", height: "45vh", backgroundColor: "#15171c"}}/>
      </>
  );
}