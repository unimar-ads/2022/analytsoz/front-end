export default  {
    img: {
        position: "absolute",
        marginLeft: "auto",
        marginRight: "auto",
        left: 0,
        right: 0,
        textAlign: "center",
        marginTop: 200,
        width: 90,
        height: 90
    }
}