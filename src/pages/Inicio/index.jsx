import React from "react";
import "bootstrap/dist/css/bootstrap.min.css"

import FaturamentoSemanal from "./Faturamentos/Semanal";
import FaturamentoMensal from "./Faturamentos/Mensal";

export default function Inicio() {
    return (
        <div style={{flexDirection: "row", display: "flex", flex: 1, flexWrap: "wrap"}}>
            <FaturamentoSemanal/>
            <FaturamentoMensal/>
        </div>
    );
}