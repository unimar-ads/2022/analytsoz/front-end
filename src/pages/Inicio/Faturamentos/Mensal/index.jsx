import Chart from "react-apexcharts";
import { Paper, Stack } from "@mui/material";
import { useEffect, useState } from "react";

import { Spinner } from "react-activity";
import "react-activity/dist/library.css";

import { vendasProdutoServico } from "../../../../controllers/FaturamentoController";

export default function FaturamentoMensal() {
    const [loading, setLoading] = useState(true);
    const [dados, setDados] = useState([]);

    useEffect(() => {
        async function getDados() {
            const result = await vendasProdutoServico(30);
            setDados(result);
            setLoading(false);
        }

        getDados();
    }, []);

    return (
        <>
            <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                <Paper sx={{ padding: 2, margin: 3, minHeight: 700, maxWidth: 950}}>
                    <h2 style={{marginBottom: 50}}>Faturamento mensal</h2>
                        {!loading  ?
                            <Stack spacing={2}>
                                <div style={{flexDirection: "row", display: "flex", flex: 1, flexWrap: "wrap"}}>
                                    <Chart
                                        type="pie"
                                        width={350}
                                        height={350}
                                        series={dados.valorganho.values}
                                        options={{
                                            title: {
                                                text: `Total ganho (R$${dados.valorganho.values.reduce((a, b) => a + b, 0)})`,
                                                align: 'left',
                                                margin: 10,
                                                offsetX: 0,
                                                offsetY: 0,
                                                floating: false,
                                                style: {
                                                fontSize:  '14px',
                                                fontWeight:  'bold',
                                                fontFamily:  undefined,
                                                color:  '#263238'
                                                },
                                            },
                                            labels: dados.valorganho.keys
                                        }}
                                    />

                                    <Chart
                                        type="pie"
                                        height={350}
                                        width={350}
                                        series={dados.vendas.values}
                                        options={{
                                            title: {
                                                text: `Total de vendas (${dados.vendas.values.reduce((a, b) => a + b, 0)})`,
                                                align: 'left',
                                                margin: 10,
                                                offsetX: 0,
                                                offsetY: 0,
                                                floating: false,
                                                style: {
                                                fontSize:  '14px',
                                                fontWeight:  'bold',
                                                fontFamily:  undefined,
                                                color:  '#263238'
                                                },
                                            },
                                            labels: dados.vendas.keys
                                        }}
                                    />
                                </div>

                                <div style={{flexDirection: "row", display: "flex", flex: 1, flexWrap: "wrap"}}>
                                    <Chart
                                        series={[
                                            {
                                                name: "",
                                                data: dados.maisvendidos.produtos.values
                                            },
                                        ]}
                                        options={{
                                            title: {
                                                text: "Produtos mais vendidos",
                                                align: 'left',
                                                margin: 10,
                                                offsetX: 0,
                                                offsetY: 0,
                                                floating: false,
                                                style: {
                                                fontSize:  '14px',
                                                fontWeight:  'bold',
                                                fontFamily:  undefined,
                                                color:  '#263238'
                                                },
                                            },
                                            xaxis: {categories: dados.maisvendidos.produtos.keys}
                                        }}
                                        type="bar"
                                        width={350}
                                        height={350}
                                    />

                                    <Chart
                                        series={[
                                            {
                                                name: "Serviço",
                                                data: dados.maisvendidos.servicos.values
                                            }
                                        ]}
                                        options={{
                                            title: {
                                                text: "Serviços mais vendidos",
                                                align: 'left',
                                                margin: 10,
                                                offsetX: 0,
                                                offsetY: 0,
                                                floating: false,
                                                style: {
                                                fontSize:  '14px',
                                                fontWeight:  'bold',
                                                fontFamily:  undefined,
                                                color:  '#263238'
                                                },
                                            },
                                            xaxis: {categories: dados.maisvendidos.servicos.keys}
                                        }}
                                        type="bar"
                                        width={300}
                                        height={300}
                                    />
                                </div>
                            </Stack>
                    : 
                        <div style={{display: "flex",  justifyContent: "center", alignItems: "center", height: "100vh"}}>
                            <Spinner style={{width: 2, height: 2}}/> 
                        </div>
                    }
                </Paper>
            </div>
        </>
    );
}