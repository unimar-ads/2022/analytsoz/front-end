import React from "react";

import { useFormik } from "formik";
import * as yup from "yup";

import { IoIosPaper } from "react-icons/io";

import { Paper, Stack } from "@mui/material";

import { CustomButton, CustomInput } from "../../components/CustomWidgets";

import { insertServico } from "../../controllers/ServicoController";

import { useSnackbar } from "notistack";

const validationSchema = yup.object({
    descricao: yup.string().trim().required("Informe o nome do serviço"),
    valor: yup.string().trim().required("Informe o valor do serviço"),
    tempo_gasto: yup.string().trim().required("Informe o tempo gasto")
});

export default function CadastroServico() {
    const { enqueueSnackbar } = useSnackbar();
    const [loading, setLoading] = React.useState(false);

    const sendInputInfo = async(values) => {
        if (!loading)
            setLoading(true);
            const response = await insertServico(values);
            setLoading(false);

            if (response) 
                enqueueSnackbar("Serviço cadastrado com sucesso", {
                    variant: "success",
                });
            else
                enqueueSnackbar("Ocorreu um erro ao cadastrar o serviço", {
                    variant: "error",
                });
    }

    const formik = useFormik({
        initialValues: {
          descricao: "",
          valor: "",
          tempo_gasto: ""
        },
        onSubmit: sendInputInfo,
        validationSchema: validationSchema
    });

    return (
        <>
            <form onSubmit={formik.handleSubmit}>
                <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                    <IoIosPaper style={{width: 26, height: 26, marginRight: 5}}/>
                    <h4>Cadastro de serviço</h4>
                </Paper>

                <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                    <Paper sx={{ padding: 4, margin: 2, width: 700}}>
                        <Stack spacing={2}>
                            <CustomInput
                                label="Nome do serviço"
                                name="descricao"
                                onChange={formik.handleChange}  
                                error={formik.touched.descricao && Boolean(formik.errors.descricao)}
                                helperText={formik.touched.descricao && formik.errors.descricao}
                            />
                            <CustomInput 
                                label="Valor do serviço"
                                name="valor"
                                onChange={formik.handleChange} 
                                error={formik.touched.valor && Boolean(formik.errors.valor)}
                                helperText={formik.touched.valor && formik.errors.valor}
                            />
                            <CustomInput 
                                label="Tempo médio(em horas) que leva para executar o serviço" 
                                name="tempo_gasto"
                                onChange={formik.handleChange}
                                error={formik.touched.tempo_gasto && Boolean(formik.errors.tempo_gasto)}
                                helperText={formik.touched.tempo_gasto && formik.errors.tempo_gasto}
                            />
                            <CustomButton type="submit" isLoading={loading} placeholder={"Cadastrar"} sx={{width: 200}} variant="contained"/>
                        </Stack>
                    </Paper>
                </div>
            </form>
        </>
    );
}