import React from "react";

import { useFormik } from "formik";
import * as yup from "yup";

import { Paper, Stack } from "@mui/material";

import { MdSell } from "react-icons/md";

import { CustomButton, CustomInput } from "../../components/CustomWidgets";

import { insertProduto } from "../../controllers/ProdutoController";

import { useSnackbar } from "notistack";

const validationSchema = yup.object({
    nome: yup.string().trim().required("Informe o nome do produto"),
    valor: yup.string().trim().required("Informe o valor do produto")
});

export default function CadastroProduto() {
    const { enqueueSnackbar } = useSnackbar();
    const [loading, setLoading] = React.useState(false);

    const sendInputInfo = async(values) => {
        if (!loading)
            setLoading(true);
            const response = await insertProduto(values);
            setLoading(false);

            if (response) 
                enqueueSnackbar("Produto cadastrado com sucesso", {
                    variant: "success",
                });
            else
                enqueueSnackbar("Ocorreu um erro ao cadastrar o produto", {
                    variant: "error",
                });
    }

    const formik = useFormik({
        initialValues: {
          nome: "",
          valor: ""
        },
        onSubmit: sendInputInfo,
        validationSchema: validationSchema
    });

    return (
        <>
            <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                <MdSell style={{width: 25, height: 25, marginRight: 5}}/>
                <h4>Cadastro de produto</h4>
            </Paper>
            <form onSubmit={formik.handleSubmit}>
                <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                    <Paper sx={{ padding: 4, margin: 2, width: 700}}>
                        <Stack spacing={2}>
                            <CustomInput 
                                label="Nome do produto" 
                                name="nome" 
                                onChange={formik.handleChange} 
                                error={formik.touched.nome && Boolean(formik.errors.nome)}
                                helperText={formik.touched.nome && formik.errors.nome}
                            />
                            <CustomInput 
                                label="Valor"
                                name="valor" 
                                onChange={formik.handleChange}
                                error={formik.touched.valor && Boolean(formik.errors.valor)}
                                helperText={formik.touched.valor && formik.errors.valor}
                                type="number"
                            />
                            <CustomButton type="submit" isLoading={loading} placeholder={"Cadastrar"} sx={{width: 200}} variant="contained"/>
                        </Stack>
                    </Paper>
                </div>
            </form>
        </>
    );
}