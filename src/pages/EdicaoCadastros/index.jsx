import React from "react";

import { Paper, Stack, ThemeProvider, MenuItem, InputLabel } from "@mui/material";
import Select from "@mui/material/Select";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";

import { Spinner } from "react-activity";

import { AiTwotoneEdit } from "react-icons/ai";

import FormTheme from "../../components/FormTheme";

import { getAllProdutos, editProduto } from "../../controllers/ProdutoController";
import { getAllServicos, editServico } from "../../controllers/ServicoController";
import { getAllClientes, editCliente } from "../../controllers/ClienteController";

import { useSnackbar } from "notistack";

import DeleteOutline from "@mui/icons-material/DeleteOutline";
import IconButton from "@mui/material/IconButton";

const currencyFormatter = new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
});

const brlPrice = {
    type: 'number',
    width: 130,
    valueFormatter: ({ value }) => currencyFormatter.format(value),
    cellClassName: 'font-tabular-nums',
};

const getters = [
    getAllProdutos,
    getAllServicos,
    getAllClientes
];

const updaters = [
    editProduto,
    editServico,
    editCliente
]

export default function EdicaoCadastros() {
    const [gridSelecionado, setGridSelecionado] = React.useState(0);
    const [data, setData] = React.useState([]);
    const { enqueueSnackbar } = useSnackbar();

    const colunas = [
        [
            { field: "nome", headerName: "Nome", editable: true },
            { field: "valor", headerName: "Valor", editable: true, ...brlPrice },
            {
                field: "action",
                headerName: "",
                sortable: false,
                renderCell: (item) => (
                    <>
                        <IconButton onClick={() => deleteItem(item.id)}>
                            <DeleteOutline/>
                        </IconButton>
                    </>
                )
            }
        ],
        [
            { field: "descricao", headerName: "Nome", editable: true },
            { field: "valor", headerName: "Valor", editable: true, ...brlPrice },
            { field: "tempo_gasto", headerName: "Tempo gasto", editable: true },
            {
                field: "action",
                headerName: "",
                sortable: false,
                renderCell: (item) => (
                    <>
                        <IconButton onClick={() => deleteItem(item.id)}>
                            <DeleteOutline/>
                        </IconButton>
                    </>
                )
            }
        ],
        [
            { field: "nome", headerName: "Nome", editable: true },
            { field: "cpfcnpj", headerName: "CPF/CNPJ", editable: true },
            { field: "email", headerName: "E-mail", editable: true },
            { field: "cidade", headerName: "Cidade", editable: true },
            { field: "bairro", headerName: "Bairro", editable: true },
            { field: "logradouro", headerName: "Logradouro", editable: true },
            { field: "cep", headerName: "CEP", editable: true },
            {
                field: "action",
                headerName: "",
                sortable: false,
                renderCell: (item) => (
                    <>
                        <IconButton onClick={() => deleteItem(item.id)}>
                            <DeleteOutline/>
                        </IconButton>
                    </>
                )
            }
        ]
    ];

    React.useEffect(() => {
        const fetchData = async () => {
            setData([]);
            const res = await getters[gridSelecionado]();

            setData(res);
        } 

        fetchData();
    }, [gridSelecionado]);

    const handleChange = (event) => {
        setGridSelecionado(event.target.value);
    };

    async function updateItem(id, field, value) {
        const dict = {}
        dict[field] = value;
        
        const res = await updaters[gridSelecionado](id, dict);

        if (res) 
            enqueueSnackbar("Item alterado com sucesso", {
                variant: "success",
            });
    }

    function removeDataGridData(id) {
        setData(data.filter((data) => data.id !== id));
    }

    async function deleteItem(id) {
        const res = await updaters[gridSelecionado](id, {ativo: false});

        if (res) 
            removeDataGridData(id);
            enqueueSnackbar("Item apagado com sucesso", {
                variant: "success",
            });
    }

    return (
        <>
            <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                <AiTwotoneEdit style={{width: 26, height: 26, marginRight: 5}}/>
                <h4>Edição de cadastros</h4>
            </Paper>
            <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                <Paper sx={{ padding: 4, margin: 2, width: 1200}}>
                    <Stack spacing={2}>
                        <ThemeProvider theme={FormTheme}>
                            <InputLabel id="test-select-label">Opção</InputLabel>
                            <Select
                                value={gridSelecionado}
                                onChange={handleChange}
                                labelId="test-select-label"
                                label="Opção"
                            >
                                <MenuItem value={0}>Produtos</MenuItem>
                                <MenuItem value={1}>Serviços</MenuItem>
                                <MenuItem value={2}>Clientes</MenuItem>
                            </Select>
                            <div style={{ height: 400 }}>
                                {data.length > 0 ?
                                    <DataGrid
                                        disableColumnFilter
                                        disableColumnSelector
                                        disableDensitySelector
                                        disableSelectionOnClick   
                                        onCellEditCommit={(item) => updateItem(item.id, item.field, item.value)}
                                        rowsPerPageOptions={[]}
                                        components={{ Toolbar: GridToolbar }}
                                        componentsProps={{
                                            toolbar: {
                                                showQuickFilter: true,
                                                quickFilterProps: { debounceMs: 500 },
                                            },
                                        }}
                                        rows={data}
                                        columns={colunas[gridSelecionado]}
                                    />
                                : 
                                <div style={{display: "flex",  justifyContent: "center", alignItems: "center", height: "45vh"}}>
                                    <Spinner style={{width: 50, height: 50}}/> 
                                </div>
                               }
                            </div>
                        </ThemeProvider>
                    </Stack>
                </Paper>
            </div>
        </>
    );
}