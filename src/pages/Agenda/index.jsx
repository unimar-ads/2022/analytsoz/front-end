import React from "react";

import { AiFillSchedule } from "react-icons/ai";
import { Paper } from "@mui/material";

import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  DayView,
  Appointments,
} from '@devexpress/dx-react-scheduler-material-ui';

const currentDate = '2018-11-01';
const schedulerData = [
  { startDate: '2018-11-01T09:45', endDate: '2018-11-01T11:00', title: 'Meeting' },
  { startDate: '2018-11-01T12:00', endDate: '2018-11-01T13:30', title: 'Go to a gym' },
]

export default function Agenda() {
    return (
        <>
            <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                <AiFillSchedule style={{width: 26, height: 26, marginRight: 5}}/>
                <h4>Agenda</h4>
            </Paper>
            <Paper sx={{margin:5, marginLeft: 50, marginRight: 50}}>
                <Scheduler
                    locale={"pt-BR"}
                    data={schedulerData}
                    width={200}
                >
                    <ViewState
                        currentDate={currentDate}
                    />
                    <DayView
                        startDayHour={9}
                        endDayHour={14}
                    />
                    <Appointments/>
                </Scheduler>
            </Paper>
        </>
    )
}