import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import cayque from "../../assets/cayque.png";
import styles from "./styles.js";

export default function Inicio() {
    return (
        <div style={{display: "flex",  justifyContent: "center", alignItems: "center", fontFamily: "roboto", fontSize: 80}}>
         Criadores:
                <img style={styles.img} src={cayque}></img>
        </div>
    );
}