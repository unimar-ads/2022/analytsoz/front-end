import React from "react";

import { useFormik } from "formik";
import * as yup from "yup";

import { insertCliente } from "../../controllers/ClienteController";

import { IoMdPersonAdd } from "react-icons/io";
import { Paper, Stack } from "@mui/material";

import { CustomButton, CustomInput } from "../../components/CustomWidgets";

import { useSnackbar } from "notistack";

const validationSchema = yup.object({
    nome: yup.string().trim().required("Informe o nome do cliente"),
    cpfcnpj: yup.string().trim().required("Informe o CPF ou CPNJ do cliente"),
    email: yup.string().email("Informe um e-mail válido").required("Informe o e-mail do cliente"),
    cidade: yup.string().trim().required("Informe a cidade"),
    bairro: yup.string().trim().required("Informe o bairro"),
    logradouro: yup.string().trim().required("Informe o logradouro"),
    cep: yup.string().trim().required("Informe o CEP")
});

export default function CadastroCliente() {
    const { enqueueSnackbar } = useSnackbar();
    const [loading, setLoading] = React.useState(false);

    const sendInputInfo = async(values) => {
        if (!loading)
            setLoading(true);
            const response = await insertCliente(values);
            setLoading(false);

            if (response) 
                enqueueSnackbar("Cliente cadastrado com sucesso", {
                    variant: "success",
                });
            else
                enqueueSnackbar("Ocorreu um erro ao cadastrar o cliente", {
                    variant: "error",
                });

    }

    const formik = useFormik({
        initialValues: {
            nome: "",
            cpfcnpj: "",
            email: "",
            cidade: "",
            bairro: "",
            logradouro: "",
            cep: ""
        },
        onSubmit: sendInputInfo,
        validationSchema: validationSchema
    });

    return (
        <>
            <form onSubmit={formik.handleSubmit}>
                <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                    <IoMdPersonAdd style={{width: 26, height: 26, marginRight: 5}}/>
                    <h4>Cadastro de cliente</h4>
                </Paper>

                <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                    <Paper sx={{ padding: 4, margin: 2, width: 700}}>
                        <Stack spacing={2}>
                            <CustomInput 
                                name="nome"
                                label="Nome do cliente"
                                onChange={formik.handleChange} 
                                error={formik.touched.nome && Boolean(formik.errors.nome)}
                                helperText={formik.touched.nome && formik.errors.nome} 
                            />
                            <CustomInput 
                                name="cpfcnpj"
                                label="CPF/CNPJ"
                                onChange={formik.handleChange}
                                error={formik.touched.cpfcnpj && Boolean(formik.errors.cpfcnpj)}
                                helperText={formik.touched.cpfcnpj && formik.errors.cpfcnpj}
                            />
                            <CustomInput 
                                name="email"
                                label="E-mail" 
                                onChange={formik.handleChange} 
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                            />
                            <CustomInput 
                                name="cidade"
                                label="Cidade"
                                onChange={formik.handleChange} 
                                error={formik.touched.cidade && Boolean(formik.errors.cidade)}
                                helperText={formik.touched.cidade && formik.errors.cidade}  
                            />
                            <CustomInput 
                                name="bairro"
                                label="Bairro"
                                onChange={formik.handleChange} 
                                error={formik.touched.bairro && Boolean(formik.errors.bairro)}
                                helperText={formik.touched.bairro && formik.errors.bairro}
                            />
                            <CustomInput
                                name="logradouro"
                                label="Logradouro" 
                                onChange={formik.handleChange} 
                                error={formik.touched.logradouro && Boolean(formik.errors.logradouro)}
                                helperText={formik.touched.logradouro && formik.errors.logradouro} 
                            />
                            <CustomInput
                                name="cep"
                                label="CEP"  
                                onChange={formik.handleChange} 
                                error={formik.touched.cep && Boolean(formik.errors.cep)}
                                helperText={formik.touched.cep && formik.errors.cep} 
                            />
                            <CustomButton type="submit" isLoading={loading} placeholder={"Cadastrar"} sx={{width: 200}} variant="contained"/>
                        </Stack>
                    </Paper>
                </div>
            </form>
        </>
    );
}