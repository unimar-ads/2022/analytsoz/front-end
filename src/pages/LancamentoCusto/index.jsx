import React from "react";

import { useFormik } from "formik";
import * as yup from "yup";

import { Paper, Stack } from "@mui/material";

import { CustomButton, CustomInput } from "../../components/CustomWidgets";

import { MdSell } from "react-icons/md";

import { insertCusto } from "../../controllers/CustoController";

import { useSnackbar } from "notistack";

const validationSchema = yup.object({
    data_lancamento: yup.string().trim().required("Informe a data de lançamento"),
    descricao: yup.string().trim().required("Informe a descrição"),
    valor: yup.string().trim().required("Informe o valor"),
    fornecedor: yup.string().trim().required("Informe fornecedor")
});

export default function LancamentoCusto() {
    const { enqueueSnackbar } = useSnackbar();
    const [loading, setLoading] = React.useState(false);

    const sendInputInfo = async(values) => {
        if (!loading)
            setLoading(true);
            const response = await insertCusto(values);
            setLoading(false);
        
            if (response) 
                enqueueSnackbar("Custo lançado com sucesso", {
                    variant: "success",
                });
            else
                enqueueSnackbar("Ocorreu um erro ao lançar o custo", {
                    variant: "error",
                });
    }

    const formik = useFormik({
        initialValues: {
            data_lancamento: "",
            descricao: "",
            valor: "",
            fornecedor: ""
        },
        onSubmit: sendInputInfo,
        validationSchema: validationSchema
    });

    return (
        <>
            <form onSubmit={formik.handleSubmit}>
                <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", flexGrow:1, justifyContent: "center", alignItems: "center"}}>
                    <MdSell style={{width: 26, height: 26, marginRight: 5}}/>
                    <h4>Lançamento de custo</h4>
                </Paper>

                <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                    <Paper sx={{ padding: 4, margin: 2, width: 700}}>
                        <Stack spacing={2}>
                            <CustomInput 
                                InputLabelProps={{ shrink: true }}
                                label="Data de lançamento"
                                name="data_lancamento"
                                type="date" 
                                onChange={formik.handleChange} 
                                error={formik.touched.data_lancamento && Boolean(formik.errors.data_lancamento)}
                                helperText={formik.touched.data_lancamento && formik.errors.data_lancamento}
                            />
                            <CustomInput 
                                label="Descrição" 
                                name="descricao"
                                onChange={formik.handleChange} 
                                error={formik.touched.descricao && Boolean(formik.errors.descricao)}
                                helperText={formik.touched.descricao && formik.errors.descricao}
                            />
                            <CustomInput 
                                label="Valor" 
                                name="valor"
                                onChange={formik.handleChange} 
                                error={formik.touched.valor && Boolean(formik.errors.valor)}
                                helperText={formik.touched.valor && formik.errors.valor}
                            />
                            <CustomInput 
                                label="Fornecedor"
                                name="fornecedor"
                                onChange={formik.handleChange}  
                                error={formik.touched.fornecedor && Boolean(formik.errors.fornecedor)}
                                helperText={formik.touched.fornecedor && formik.errors.fornecedor}
                            />
                            <CustomButton type="submit" isLoading={loading} placeholder={"Cadastrar"} sx={{width: 200}} variant="contained"/>
                        </Stack>
                    </Paper>
                </div>
            </form>
        </>
    );
}