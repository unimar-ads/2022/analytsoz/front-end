import React from "react";
import { Spinner } from "react-activity";

import { DataGrid } from "@mui/x-data-grid";
import { Autocomplete, TextField, Button, Paper, Stack, IconButton } from "@mui/material";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

import { Bounce } from "react-activity";
import { ThemeProvider } from "@mui/material/styles";

import FormTheme from "../../components/FormTheme";

import { MdSell } from "react-icons/md";

import { useSnackbar } from "notistack";

import { getAllClientes } from "../../controllers/ClienteController";
import { getAllProdutos } from "../../controllers/ProdutoController";
import { getAllServicos } from "../../controllers/ServicoController";
import { insertVenda } from "../../controllers/VendaController";

import { CustomButton } from "../../components/CustomWidgets";

export default function RegistroVenda() {
    const { enqueueSnackbar } = useSnackbar();
    const [loadingData, setloadingData] = React.useState(true);

    const [clientList, setClientList] = React.useState([]);
    const [produtoList, setProdutoList] = React.useState([]);
    const [servicoList, setServicoList] = React.useState([]);
    const [dataGridRows, setdataGridRows] = React.useState([]);

    const [loading, setLoading] = React.useState(false);

    const [clienteSelecionado, setclienteSelecionado] = React.useState();

    function verifyErrors() {
        var pass = true;

        if (dataGridRows.length === 0) {
            enqueueSnackbar("É necessário ter pelo menos 1 item para registrar uma venda",  {variant: "error"});
            return false;
        } else if(!clienteSelecionado) {
            enqueueSnackbar("É necessário selecionar um cliente para cadastrar uma venda",  {variant: "error"});
            return false;
        }

        
        for (var i = 0; i < dataGridRows.length; i ++) {
            const item = dataGridRows[i];

            if (item.qtd <= 0) {
                enqueueSnackbar(`A quantidade do item ${item.nome} precisa ser maior que 0`,  {variant: "error"});
                pass = false;
                break;
            }
        }

        return pass;
    }

    async function sendRequest(e) {
        e.preventDefault();

        if (verifyErrors() && !loading) {
            setLoading(true);
            const response = await insertVenda({
                cliente_venda: clienteSelecionado,
                itens: dataGridRows
            });

            setLoading(false);

            if (response) {
                enqueueSnackbar("Venda registrada com sucesso",  {variant: "success"});
            } else {
                enqueueSnackbar("Falha ao registrar a venda",  {variant: "error"});
            }
        }
    }

    function insertDataGridData(id, nome, categoria, qtd) {
        setdataGridRows([...dataGridRows, {id, nome, categoria, qtd}])
    }

    function removeDataGridData(item) {
        setdataGridRows(dataGridRows.filter((data) => data.id !== item.id));
    }

    function updateDataGridRow(id, newvalue) {
        const backupData = [...dataGridRows];

        for (var i = 0; i < backupData.length; i++) {
            const item = backupData[i];

            if (item.id === id) {
                backupData[i].qtd = newvalue;
                setdataGridRows(backupData);
                break;
            }
        }
    }

    React.useEffect(() => {
        async function request() {
            const clientes = await getAllClientes();
            const produtos = await getAllProdutos();
            const servicos = await getAllServicos();

            setClientList(clientes);
            setProdutoList(produtos);
            setServicoList(servicos);

            setloadingData(false);
        }

        request();
    }, [])

    const columns = [
        { field: "nome", headerName: "Nome", width: 200, editable: false, sortable: false, },
        { field: "categoria", headerName: "Categoria", width: 80, editable: false, sortable: false, },
        { field: "qtd", headerName: "Quantidade", width: 100, type: "number", editable: true, sortable: false, },
        {
            field: "action",
            headerName: "",
            width: 50,
            sortable: false,
    
            renderCell: (item) => (
                <>
                    <IconButton onClick={() => {removeDataGridData(item)}}>
                        <DeleteOutlineIcon/>
                    </IconButton>
                </>
            )
        }
    ];

    return (
        <>
            <Paper sx={{ padding: 4, flexDirection: "row", flex:1, display:"flex", justifyContent: "center", alignItems: "center"}}>
                    <MdSell style={{width: 26, height: 26, marginRight: 5}}/>
                    <h4>Registro de venda</h4>
            </Paper>
            
            {loadingData ? 
                <div style={{display: "flex",  justifyContent: "center", alignItems: "center", height: "20vh"}}>
                    <Spinner style={{width: 50, height: 50}}/> 
                </div> 
            :
                <form>
                    <div style={{display: "flex",  justifyContent: "center", alignItems: "center"}}>
                        <Paper sx={{ padding: 4, margin: 2, width: 700}}>
                            <Stack spacing={2}>
                                <ThemeProvider theme={FormTheme}>
                                    <Autocomplete
                                        options={clientList}
                                        onChange={(event, value) => value && setclienteSelecionado(value.id)}
                                        getOptionLabel={option => option.nome}
                                        renderInput={(params) => <TextField {...params} label="Cliente" />}
                                        renderOption={(props, option) => {
                                            return (
                                              <li {...props} key={option.id}>
                                                {option.nome}
                                              </li>
                                            );
                                        }}
                                    />

                                    <Autocomplete
                                        options={produtoList}
                                        getOptionLabel={option => option.nome}
                                        onChange={(event, value) => value && insertDataGridData(value.id, value.nome, "Produto", 0)}
                                        renderInput={(params) => <TextField {...params} label="Produto" />}
                                        renderOption={(props, option) => {
                                            return (
                                              <li {...props} key={option.id}>
                                                {option.nome}
                                              </li>
                                            );
                                        }}
                                    />

                                    <Autocomplete
                                        options={servicoList}
                                        getOptionLabel={option => option.descricao}
                                        onChange={(event, value) => value && insertDataGridData(value.id ,value.descricao, "Serviço", 0)}
                                        renderInput={(params) => <TextField {...params} label="Serviço" />}
                                        renderOption={(props, option) => {
                                            return (
                                              <li {...props} key={option.id}>
                                                {option.descricao}
                                              </li>
                                            );
                                        }}
                                    />
                                    <div style={{ height: 400, maxWidth: 450 }}>
                                        <DataGrid
                                            rows={dataGridRows}
                                            columns={columns}
                                            onCellEditCommit={(item) => updateDataGridRow(item.id, item.value)}
                                            rowsPerPageOptions={[]}
                                        />
                                    </div>
                                    <CustomButton placeholder={"Cadastrar"} isLoading={loading} sx={{width: 200}} onClick={sendRequest} variant="contained"/>
                                </ThemeProvider>
                            </Stack>
                        </Paper>
                    </div>
                </form>
            }
        </>
    );
}