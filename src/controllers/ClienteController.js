import api from "../services/api";

export default async function getCliente(id) {
    try {
        const result = await api.get(`cliente/${id}`);
        return result.data;
    } catch(error) {
        return false;
    }  
}

export async function getAllClientes() {
    try {
        const result = await api.get("cliente");
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function editCliente(id, dados) {
    try {
        const result = await api.put(`cliente/${id}`, dados);
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function insertCliente(dados) {
    try {
        await api.post("cliente", dados);
        return true;
    } catch(error) {
        return false;
    }
}