import api from "../services/api";

export async function vendasProdutoServico(dias) { 
    try {
        const res = await api.get(`faturamento/vendasProdutoServico/${dias}`);

        return res.data;
    } catch(error) {
        return false;
    }
}