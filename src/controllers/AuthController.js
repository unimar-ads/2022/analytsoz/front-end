import api from "../services/api";

export async function login(usuario, senha) {
    try {
        const response = await api.get("usuario/login", {
            auth: {
                username: usuario,
                password: senha
            }
        })

        localStorage.setItem("nome", response.data.nome);
        return true;
    } catch(error) {
        return false;
    }
}

export async function logout() {
    try {
        await api.get("usuario/logout")
        return true;
    } catch(error) {
        return false;
    }
}

export async function checkAuth() {
    try {
        await api.get("usuario/checkAuth");
        return true;
    } catch(error) {
        return false;
    }
}