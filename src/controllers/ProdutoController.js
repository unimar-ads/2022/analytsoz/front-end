import api from "../services/api";

export default async function getProduto() {
        
}

export async function getAllProdutos() {
    try {
        const result = await api.get("produto");
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function editProduto(id, dados) {
    try {
        const result = await api.put(`produto/${id}`, dados);
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function insertProduto(dados) {
    try {
        await api.post("produto", dados);
        return true;
    } catch(error) {
        return false;
    }
}