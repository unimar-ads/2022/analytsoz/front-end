import api from "../services/api";

export default async function getServico() {
        
}

export async function getAllServicos() {
    try {
        const result = await api.get("servico");
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function editServico(id, dados) {
    try {
        const result = await api.put(`servico/${id}`, dados);
        return result.data;
    } catch(error) {
        return false;
    }
}

export async function insertServico(dados) {
    try {
        await api.post("servico", dados);
        return true;
    } catch(error) {
        return false;
    }
}