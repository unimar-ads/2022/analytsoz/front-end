import React from "react";
import { Route, Routes, Navigate, Outlet } from "react-router-dom";
import CadastroServico from "./pages/CadastroServico";
import CadastroProduto from "./pages/CadastroProduto";
import Login from "./pages/Login";
import CadastroCliente from "./pages/CadastroCliente";
import RegistroVenda from "./pages/RegistroVenda";
import Inicio from "./pages/Inicio";
import PrivateRoute from "./components/PrivateRoute";
import Footer from "./components/Footer";
import Sidebar from "./components/Navbar";
import LancamentoCusto from "./pages/LancamentoCusto";
import EdicaoCadastros from "./pages/EdicaoCadastros";
import Agenda from "./pages/Agenda";
import Criacao from "./pages/Criacao";
import Visao from "./pages/Visao";

export default () => {
    return (
        <Routes>
            <Route path="*" element={<Navigate to="/inicio" replace />} />
                <Route exact path="/login" element={ <Login/>  }/>
                <Route element={(<>  <Sidebar/> <PrivateRoute/>  <Outlet/> </>)}>
                    <Route exact path="/inicio" element={ <Inicio/> }/>            
                    <Route exact path="/cadastroServico" element={ <CadastroServico/> }/>
                    <Route exact path="/cadastroProduto" element={ <CadastroProduto/> }/>
                    <Route exact path="/cadastroCliente" element={ <CadastroCliente/> }/>
                    <Route exact path="/registroVenda" element={ <RegistroVenda/> }/>
                    <Route exact path="/lancamentoCusto" element={ <LancamentoCusto/> }/>
                    <Route exact path="/edicaoCadastros" element={ <EdicaoCadastros/> }/>
                    <Route exact path="/agenda" element={ <Agenda/> }/>
                    <Route exact path="/criacao" element={ <Criacao/> }/>
                    <Route exact path="/visao" element={ <Visao/> }/>
                </Route>
        </Routes>
    );
}