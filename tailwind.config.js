
module.exports = {
  mode: "jit",
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "light-white": "rgba(255,255,255,0.18)"
      }
    },
  },
  plugins: [],
};
